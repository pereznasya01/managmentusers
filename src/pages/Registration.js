import React, { useState, Fragment } from 'react';
import { useNavigate } from 'react-router-dom';
import UserService from '../service/UserService';
import "./home.css";
import Swal from 'sweetalert2';

const Registration = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: '',
        role: '',
        city: ''
    });
    const [errors, setErrors] = useState({});

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const hasConsecutiveLetters = (password) => {
        for (let i = 0; i < password.length - 1; i++) {
            const currentChar = password.charCodeAt(i);
            const nextChar = password.charCodeAt(i + 1);
            if (nextChar === currentChar + 1) {
                return true;
            }
        }
        return false;
    };

    const validatePassword = (password) => {
        const errors = {};
        if (password.length < 8) {
            errors.length = "La contraseña debe tener al menos 8 caracteres";
        }
        if (!/[A-Z]/.test(password)) {
            errors.uppercase = "La contraseña debe tener al menos una letra mayúscula";
        }
        if (!/[a-z]/.test(password)) {
            errors.lowercase = "La contraseña debe tener al menos una letra minúscula";
        }
        if (!/[0-9]/.test(password)) {
            errors.number = "La contraseña debe tener al menos un número";
        }
        if (!/[!@#$%^&*]/.test(password)) {
            errors.specialChar = "La contraseña debe tener al menos un carácter especial";
        }
        if (hasConsecutiveLetters(password)) {
            errors.consecutive = "La contraseña no debe contener letras consecutivas del abecedario";
        }
        setErrors(errors);
        return Object.keys(errors).length === 0;
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (validatePassword(formData.password)) {
            try {
                await UserService.register(formData);
                setFormData({
                    name: '',
                    email: '',
                    password: '',
                    role: '',
                    city: ''
                });
                Swal.fire({
                    icon: 'success',
                    title: 'User registered successfully',
                    showConfirmButton: false,
                    timer: 1500
                });
                navigate('/login');
            } catch (error) {
                console.error('Error registering user:', error);
                Swal.fire({
                    icon: 'error',
                    title: 'An error occurred while registering user',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        }
    };

    return (
        <Fragment>
            <div className="row" style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="col-sm-5" style={{ display: 'flex', justifyContent: 'center' }}>
                    <div className="form-box form-container">
                        <div className="form-top">
                            <div className="form-top-left">
                                <h3>Sign up now</h3>
                                <p>Fill in the form below to get instant access:</p>
                            </div>
                            <div className="form-top-right">
                                <img src="icon/dev-icon-150.svg" alt="Icono SVG" />
                            </div>
                        </div>
                        <div className="form-bottom">
                            <form onSubmit={handleSubmit} className="registration-form form-horizontal">
                                <div className="form-group">
                                    <label htmlFor="form-name" className="sr-only">Name</label>
                                    <input
                                        type="text"
                                        name="name"
                                        placeholder="Your name..."
                                        className="form-control"
                                        id="form-name"
                                        value={formData.name}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="form-email" className="sr-only">Email</label>
                                    <input
                                        type="email"
                                        name="email"
                                        placeholder="Your email..."
                                        className="form-control"
                                        id="form-email"
                                        value={formData.email}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="form-password" className="sr-only">Password</label>
                                    <input
                                        type="password"
                                        name="password"
                                        placeholder="Password..."
                                        className="form-control"
                                        id="form-password"
                                        value={formData.password}
                                        onChange={handleInputChange}
                                        required
                                    />
                                    {errors.length && <p className="error-message">{errors.length}</p>}
                                    {errors.uppercase && <p className="error-message">{errors.uppercase}</p>}
                                    {errors.lowercase && <p className="error-message">{errors.lowercase}</p>}
                                    {errors.number && <p className="error-message">{errors.number}</p>}
                                    {errors.specialChar && <p className="error-message">{errors.specialChar}</p>}
                                    {errors.consecutive && <p className="error-message">{errors.consecutive}</p>}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="form-role" className="sr-only">Role</label>
                                    <input
                                        type="text"
                                        name="role"
                                        placeholder="Enter your role"
                                        className="form-control"
                                        id="form-role"
                                        value={formData.role}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="form-city" className="sr-only">City</label>
                                    <input
                                        type="text"
                                        name="city"
                                        placeholder="Enter your city"
                                        className="form-control"
                                        id="form-city"
                                        value={formData.city}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </div>
                                <button type="submit" className="btn">Sign me up!</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Registration;
