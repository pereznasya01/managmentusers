import React, { useState, Fragment } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import UserService from '../service/UserService.js';
import "./home.css";
import Swal from 'sweetalert2';

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
    
        try {
            const response = await fetch(`${UserService.BASE_URL}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email, password })
            });
    
            const userData = await response.json();
    
            if (userData.token) {
                localStorage.setItem('token', userData.token);
                localStorage.setItem('role', userData.role);
                    // Redirigir según el rol del usuario
                if (userData.role === 'ADMIN') {
                    navigate('/user-management-update');
                } else {
                    navigate('/profile-user');
                }
                Swal.fire({
                    icon: 'success',
                    title: 'Login successfully',
                    showConfirmButton: false,
                    timer: 1500
                });
    
            } else {
                setError(userData.message);
            }
        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'An error occurred while logging in',
                showConfirmButton: false,
                timer: 1500
            });
            setError(error.message);
            setTimeout(() => {
                setError('');
            }, 5000);
        }
    };
    

    return (
        <Fragment>
            <div className="row" style={{ display: 'flex', justifyContent: 'center' }}>
                <div className="col-sm-5" style={{ display: 'flex', justifyContent: 'center' }}>
                    <div className="form-box form-container">
                        <div className="form-top">
                            <div className="form-top-left">
                                <h3>Login to our site</h3>
                                <p>Enter username and password to log on:</p>
                            </div>
                            <div className="form-top-right">
                                <img src="icon/dev-icon-256.svg" alt="Icono SVG" />
                            </div>
                        </div>
                        <div className="form-bottom">
                            {error && <p className="error-message">{error}</p>}
                            <form onSubmit={handleSubmit} className="login-form">
                                <div className="form-group">
                                    <label htmlFor="form-username" className="sr-only">Email</label>
                                    <input type="email" name="form-username" placeholder="Email..." className="form-username form-control" id="form-username" value={email} onChange={(e) => setEmail(e.target.value)}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="form-password" className="sr-only">Password</label>
                                    <input type="password" name="form-password" placeholder="Password..." className="form-password form-control" id="form-password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                                </div>
                                <button type="submit" className="btn">Log in!</button>
                                <div className="btn-container">
                                    <Link to='/register' className="btn-link">¿No tienes cuenta? Registrate aquí</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default Login;
