import React, { useState, useEffect } from 'react';
import UserService from '../service/UserService';
import { Link } from 'react-router-dom';
import Navbar from './Navbar';

function ProfileUserPage() {
    const [profileInfo, setProfileInfo] = useState({});

    useEffect(() => {
        fetchProfileInfo();
    }, []);

    const fetchProfileInfo = async () => {
        try {
            const token = localStorage.getItem('token'); // Retrieve the token from localStorage
            if (!token) {
                throw new Error('No token found');
            }
            const response = await UserService.getYourProfileUser(token);
            setProfileInfo(response.ourUsers); // Acceder a la propiedad ourUsers
        } catch (error) {
            console.error('Error fetching profile information:', error);
        }
    };

    return (
      <>
      <Navbar />
        <div className="profile-page-container">
            <h2>Bienvenido</h2>
            {profileInfo.name && (
                <div>
                    <p>Name: {profileInfo.name}</p>
                    <p>Email: {profileInfo.email}</p>
                    <p>City: {profileInfo.city}</p>
                        <button>
                            <Link to={`/update-profile/${profileInfo.id}`}>Update This Profile</Link>
                        </button>
                    
                </div>
            )}
        </div>
      </>
    );
}

export default ProfileUserPage;

