import jwtDecode from 'jwt-decode';

class JWTUtils {
    async extractUsername(token) {
      const decodedToken = jwtDecode(token);
      return decodedToken.sub; // or whatever the username claim is in your token
    }
  }

export default JWTUtils;