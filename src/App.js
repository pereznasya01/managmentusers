import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Login from './pages/Login';
import Registration from './pages/Registration';
import ProfilePage from './pages/ProfilePage';
import UserManagementPage from './pages/UserManagementPage';
import UpdateUser from './pages/UpdateUser';
import ProfileUserPage from './pages/ProfileUserPage';
import UpdateProfile from './pages/UpdateProfile';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Registration />} />
        <Route path="/profile" element={<ProfilePage />} />
        <Route path="/profile-user" element={<ProfileUserPage />} />
        <Route path="/user-management-update" element={<UserManagementPage/>} />
        <Route path="/update-user/:userId" element={<UpdateUser />} />
        <Route path="/update-profile/:userId" element={<UpdateProfile />} />
      </Routes>
    </div>
  );
}

export default App;
